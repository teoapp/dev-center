import React from 'react';
import clsx from 'clsx';
import Link from '@docusaurus/Link';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Documentation',
    Svg: require('../../static/img/undraw_online_test_gba7.svg').default,
    description: (
      <>
        La documentation décrit pas à pas toutes les étapes d'authentification et d'utilisation de notre API. 
      </>
    ),
    btnText: 'Parcourir la documentation',
    btnUrl: 'docs/intro',
  },
  {
    title: 'Manuel de référence de l\'API',
    Svg: require('../../static/img/undraw_Reading_book_re_kqpk.svg').default,
    description: (
      <>
        Notre API RESTFul suit le standard OpenAPI pemettant une prise en main rapide et intuitive.
      </>
    ),
    btnText: 'Ouvrir le manuel de référence de l\'API',
    btnUrl: 'https://teoapp.fr/api/docs',
  },
  {
    title: 'Blog',
    Svg: require('../../static/img/undraw_elements_cipa.svg').default,
    description: (
      <>
        Retrouvez les évolutions de l'API et les nouveautés techniques de TéO dans le blog du Dev Center.
      </>
    ),
    btnText: 'Lire les articles récents',
    btnUrl: 'blog',
  },
];

function Feature({Svg, title, description, btnUrl, btnText}) {
  const btnClass = btnUrl !== '#' ? "button button--outline button--primary" : "button button--outline disabled";
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
        {btnUrl && (
        <Link
          className={btnClass}
          to={btnUrl}>
          {btnText}
        </Link>
      )}
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
