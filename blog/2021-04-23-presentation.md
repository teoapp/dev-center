---
slug: presentation
title: Présentation du Dev Center
author: Tanguy Giton
author_title: Responsable technique TéO
author_image_url: https://www.gravatar.com/avatar/655e2e74f83bd2ba1499b3f00bfc24fa
tags: [api, teo]
---

Bienvenue dans le **Dev Center**, l'espace réservé aux développeurs souhaitant intégrer TéO dans leur écosystème.

Dans un premier temps, cet espace offrira des ressources pour aider les développeurs à utiliser l'API de TéO pour certains cas d'usage, notamment :
- Afficher les informations de vos prestations sur votre site internet
- Créer des dossiers depuis un formulaire
- Proposer à vos visiteurs de prendre rendez-vous avec vos collaborateurs

L'API de TéO est également utilisée par notre équipe afin de réaliser de nouvelles applications rapidement. Par exemple [**TéOrdv**](https://rendezvous.teoapp.fr/#/demo), notre application de prise de rendez-vous en ligne.

Le **Dev Center** est un espace communautaire, vous pouvez participer à son amélioration et nous faire part des bugs rencontrés sur [Gitlab](https://gitlab.com/teoapp/dev-center).

Bonne visite !
