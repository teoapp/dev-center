/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'téo dev center',
  tagline: 'Ressources et documentation technique pour téo',
  url: 'https://dev.teoapp.fr',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.png',
  organizationName: 'TéO', // Usually your GitHub org/user name.
  projectName: 'dev-center', // Usually your repo name.
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
    localeConfigs: {
      fr: {
        label: 'Français',
        direction: 'ltr',
      },
    },
  },
  themeConfig: {
    navbar: {
      title: 'dev center',
      logo: {
        alt: 'téo dev center',
        src: 'img/logo.png',
      },
      items: [
        {
          type: 'doc',
          docId: 'intro',
          position: 'left',
          label: 'Documentation',
        },
        {
          href: 'https://teoapp.fr/api/docs',
          label: 'Référence API'
        },
        { to: '/blog', label: 'Blog' },
        { href: 'https://gitlab.com/teoapp/dev-center/-/issues', label: 'Support' },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Docs',
          items: [
            {
              label: 'Documentation',
              to: '/docs/intro',
            },
            {
              label: 'Référence API',
              to: 'https://teoapp.fr/api/docs',
            }
          ],
        },
        {
          title: 'Informations',
          items: [{
            label: 'Projet Gitlab',
            href: 'https://gitlab.com/teoapp/dev-center',
          },
          {
            label: 'Blog',
            to: 'blog',
          },
          {
            label: 'Politique de confidentialité',
            to: 'https://aide.teoapp.fr/docs/confidentialite',
          }
          ],
        },
        {
          title: 'En savoir plus',
          items: [{
            label: 'Découvrir TéO',
            href: 'https://teo-lesite.com',
          },
          {
            label: 'Suivez-nous sur LinkedIn',
            href: 'https://www.linkedin.com/company/t%C3%A9o/',
          }],
        },
      ],
      copyright: `Tous droits réservés ${new Date().getFullYear()} TéO`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl: 'https://gitlab.com/teoapp/dev-center/-/edit/master/'
        },
        blog: {
          showReadingTime: true,
          editUrl: 'https://gitlab.com/teoapp/dev-center/-/edit/master/'
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
