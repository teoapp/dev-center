---
sidebar_position: 3
---

# Pagination

La lecture des collections est soumis à une pagination, chaque page contient au maximum **30 éléments**. 

Pour lire l'ensemble des éléments d'une collection il faut faire une requête par page en ajoutant la propriété `page`.

- Avec le format de réponse `application/ld+json` (format par défaut), l’API retourne les informations concernant les pages dans la réponse :

    ```json
    {
        "@context": "/api/contexts/Group",
        "@id": "/api/production/groups",
        "@type": "hydra:Collection",
        "hydra:member": [
            ... // Au maximum 30 éléments par page
        ],
        "hydra:totalItems": 75, // Nombre total d'élements dans la collection
        "hydra:view": {
            "@id": "/api/production/groups?page=1", // URI de la page actuelle
            "@type": "hydra:PartialCollectionView",
            "hydra:first": "/api/production/groups?page=1", // URI de la première page
            "hydra:last": "/api/production/groups?page=3", // URI de la dernière page
            "hydra:next": "/api/production/groups?page=2" // URI de la page suivante
        },
        ...
    }
    ```

- Avec le format de réponse `application/json`, il est possible d'obtenir tous les éléments de la collection en effectuant une requête par page jusqu’à obtenir une réponse vide.