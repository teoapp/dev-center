---
sidebar_position: 4
---

# Statuts des groupes

Les groupes *(Group)* ont une propriété `status`, ci-dessous le tableau de correspondance entre les statuts remontés par l'API et l'état visible dans TéO.

| Propriété `status` | L'état visible dans TéO |
| --- | --- |
| OPENED | "Prévisionnel" |
| STARTED | "En cours", "Non planifié", "Attente commande" |
| CANCELED | "Refusé", "Sans suite", "Non facturable" |
| COMPLETED | "Terminé", "Abandonné" |
| CLOSED | "Soldé" |
| SUSPENDED | "Suspendu" |

:::info Terminologie différente
Il est à noter que, dans le tableau ci-dessus, est indiqué les états avec leur terminologie par défaut. Suivant votre installation TéO la terminologie des états peut être différente, si c'est le cas, contactez-nous pour connaître la table de correspondance.
:::