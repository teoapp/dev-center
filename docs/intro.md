---
sidebar_position: 1
---

# Introduction

L'API de TéO vous permet de lire et modifier les ressources de TéO depuis une application externe. Cela offre une grande flexibilité pour intégrer TéO dans un écosystème.

L'API de TéO comprend les fonctionnalités suivantes :
- la création, la récupération, la mise à jour et la suppression de ressources (CRUD)
- la validation des données
- la [pagination](pagination.md)
- le filtrage
- le support de l'hypermédia/[HATEOAS](https://en.wikipedia.org/wiki/HATEOAS) et de la négociation de contenu ([JSON-LD](https://json-ld.org/) et [Hydra](https://www.hydra-cg.com/), [JSON:API](https://jsonapi.org/), ...)
- l'[authentification](authentication.md) (avec [JWT](https://jwt.io/) et [OAuth](https://oauth.net/))
- les contrôles de sécurité et des en-têtes (testés par rapport aux recommandations de l'[OWASP](https://cheatsheetseries.owasp.org/cheatsheets/REST_Security_Cheat_Sheet.html))

## Les ressources TéO

De nouvelles ressources TéO sont régulièrement ajoutées à l'API. Ci-dessous, sont listés par module, les ressources actuellement disponibles.

Vous pouvez retrouvez ces ressources dans le [manuel de référence de l'API](https://teoapp.fr/api/docs).

#### Production

- Dossier *(Dossier)*
- Séance d'activité *(Activity)*
- Prestation *(Service)*
- Groupe *(Group)*

#### Comptabilité

- Facture *(Invoice)*

#### Chorus Pro

- Soumission de facture *(InvoiceSubmission)*
