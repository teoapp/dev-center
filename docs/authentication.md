---
sidebar_position: 2
---

# Authentification

L'**Access Token** vous permet d'identifier vos appels à l'API, ce token expire au bout de 10 minutes. 

Pour authentifier vos appels à l'API vous devez ajouter l'en-tête suivante à toutes vos requêtes :

```
Authorization: Bearer VOTRE_ACCESS_TOKEN
``` 

:::note Attention
Vous devez remplacer *VOTRE_ACCESS_TOKEN* par votre **Access Token**
:::

Pour obtenir un **Access Token** vous devez utiliser la méthode ci-dessous.

## OAuth2 *(Client Credentials Grant)*

Après en avoir fait la demande au support client, vous recevrez les informations suivantes : 

- `CLIENT_ID` (identifiant de votre application)
- `CLIENT_SECRET` (clé secrète de votre application)
- `TEO_ID` (identifiant du TéO auquel vous souhaitez accéder)

Pour obtenir un **Access Token**, exécutez la requête suivante :
#### Requête

``` http
POST https://auth.teoapp.fr/oauth2/token
 
Content-Type: application/x-www-form-urlencoded
Host: auth.teoapp.fr
Authorization: Basic <représentation en base64 de VOTRE_CLIENT_ID:VOTRE_CLIENT_SECRET>
 
grant_type=client_credentials&scope=target-entity:VOTRE_TEO_ID
```

:::note Attention

Vous devez remplacer *<représentation en base64 de VOTRE_CLIENT_ID:VOTRE_CLIENT_SECRET>* par l'encodage en base 64 de `VOTRE_CLIENT_ID:VOTRE_CLIENT_SECRET` en replaçant *VOTRE_CLIENT_ID* par votre `CLIENT_ID` et *VOTRE_CLIENT_SECRET* par votre `CLIENT_SECRET` (ne pas oublier le caractère `:`).

Et vous devez remplacer *VOTRE_TEO_ID* par votre `TEO_ID`.
:::


#### Réponse
``` json
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImhOOXpkcDVyanNQU0dEV3JraG5JMmpFQTRMSFpEUW5SIn0...",
    "expires_in": 3599,
    "scope": "target-entity:VOTRE_TEO_ID",
    "token_type": "Bearer"
}
```
