---
sidebar_position: 4
---

# Identifiants

Les ressources téo sont identifiées avec des UUID (universally unique identifier). Ces identifiants sont retournés lors des appels en lecture à l'API sous la propriété `id`. Certains UUID peuvent également être retrouvés dans l'interface de téo.

## Retrouver l'UUID d'un lieu (`location`) dans téo {#location}

Les UUID des lieux (propriété `location` dans l'API) peuvent être retrouvés dans l'interface de téo en suivant la procédure suivante :

1. Aller dans _Admin_ (icône de roue dentée)
2. Cliquer sur _Lieux & salles_
3. Cliquer sur le lieu de votre choix
4. L'UUID est visible dans le champ en lecture seule _UUID_

## Retrouver l'UUID d'une action (`action`) dans téo {#action}

Les UUID des actions (propriété `action` dans l'API) peuvent être retrouvés dans l'interface de téo en suivant la procédure suivante :

1. Aller dans _Admin_ (icône de roue dentée)
1. Cliquer sur _Gestion des actions_
1. Cliquer sur l'action de votre choix
1. L'UUID est visible à la ligne _Identifiant_

## Retrouver l'UUID d'un consultant (`consultant`) dans téo {#consultant}

Les UUID des consultants (propriété `consultant` dans l'API) peuvent être retrouvés dans l'interface de téo en suivant la procédure suivante :

1. Aller dans _Annuaire_
1. Filtrer en cochant _Personnel_
1. Cliquer sur le consultant de votre choix
1. Cliquer sur _Personnel_ dans le menu de gauche
1. L'UUID est visible à la ligne _Identifiant_

## Retrouver l'UUID d'une prestation (`service`) dans téo {#service}

Les UUID des prestations (propriété `service` dans l'API) peuvent être retrouvés dans l'interface de téo en suivant la procédure suivante :

1. Aller dans _Admin_ (icône de roue dentée)
1. Cliquer sur _Prestations_
1. Cliquer sur la prestation de votre choix
1. L'UUID est visible à la ligne _Identifiant_

## Retrouver l'UUID d'un groupe (`group`) dans téo {#group}

Les UUID des groupes (propriété `group` dans l'API) peuvent être retrouvés dans l'interface de téo en suivant la procédure suivante :

1. Ouvrez le groupe de votre choix
1. L'UUID est visible à la ligne _Identifiant_
