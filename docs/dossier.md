---
sidebar_position: 5
---

# Dossier
## Profession
La propriété `jobTitle` du `Dossier` correspond au champ "Fonction" du dossier. La valeur envoyée doit être inclus dans la liste des fonctions enregistrée dans le paramétrage de votre téo (la valeur n'est pas sensible à la casse).

Attention, l'API ne retournera pas d'erreur si la fonction n'existe pas et la propriété sera ignorée.

Pour visualiser la liste de fonctions dans votre téo, aller dans Admin > Paramétrage > Annuaire > Fonctions. Vous pouvez également ajouter une fonction en cliquant sur "Nouveau".

## Propriété `data`
Cette propriété permet d'ajouter des données supplémentaires spécifiques à la configuration de votre téo. Veuillez créer un ticket dans le support pour obtenir les informations nécessaires à l'envoi de données spécifiques.
